const User = require('../models/user')
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
const config = require('../config/index');


exports.index = async (req, res, next) => {
    const user = await User.find({});
    res.render("admin/user/index", {
        product_id: req.params.room,
        data: user,
        title: "User",
        nav: "User List",
    });
}

exports.form_register = async (req, res, next) => {
    const user = await User.find({});
    res.render("admin/user/index", {
        product_id: req.params.room,
        data: user,
        title: "User",
        nav: "User List",
    });
}


exports.authen = async (req, res, next) => {
    const user = await User.findOne({ _id: req.body.user_id }).select('role')
    console.log(user)
    //console.log(process.env.NODE_ENV)
    //var permission = menu_permission(user.role)
    return res.status(200).json({
        result: true,
        permission: user.role,
        token_type: "Bearer",
    });
}

function menu_permission(role) {
    var url = process.env.DOMAIN + '/'
    if (role == 'member') {
        return [
            { name: 'Home', tyle: 1, link: url + 'home' },
            { name: 'Purchases', tyle: 1, link: url + 'home' },
        ]
    } else if (role == 'admin') {
        return [
            { name: 'Home', tyle: 1, link: url + 'home' },
            {
                name: 'Product',
                tyle: 2,
                sub: [
                    {
                        name: 'List',
                        link: url + 'admin/product'
                    },
                    {
                        name: 'Create',
                        link: url + 'admin/product/create'
                    },
                ]
            },
            {
                name: 'Lots',
                tyle: 2,
                sub: [
                    {
                        name: 'List',
                        link: url + 'admin/product/lot/list'
                    },
                    {
                        name: 'Create',
                        link: url + 'admin/product/lot/create'
                    },
                ]
            },
            {
                name: 'User',
                tyle: 2,
                sub: [
                    {
                        name: 'List',
                        link: url + 'user'
                    },
                    {
                        name: 'Create',
                        link: url + 'user/create'
                    },
                ]
            },
            {
                name: 'Auctioneer',
                tyle: 2,
                sub: [
                    {
                        name: 'Manament',
                        link: url + 'admin/auctioneer'
                    },
                    {
                        name: 'Create',
                        link: url + 'admin/auctioneer/board'
                    },
                ]
            },
        ]
    } else {
        return []
    }
}

exports.profile = (req, res, next) => {
    res.sendfile('public/profile.html', { title: 'express' });
}


exports.admin = (req, res, next) => {
    console.log(req.user);
    return res.status(200).json({ result: true, page: 'admin' });
}

exports.register = async (req, res, next) => {
    try {
        const { name, username, password } = req.body;
        console.log('kxx : ',req.body)
        
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const error = new Error("ข้อมูลที่รับมาไม่ถูกต้อง");
            error.statusCode = 422;
            error.validation = errors.array();
            throw error;
        }

        //check email ซ้ำ
        const existUser = await User.findOne({ username: username });
        if (existUser) {
            const error = new Error("username ซ้ำ มีผู้ใช้งานแล้ว ลองใหม่อีกครั้ง");
            error.statusCode = 400;
            throw error;
        }

        let user = new User();
        user.name = name;
        user.username = username;
        user.menu = ["62bc0ca9589359f48b4037d4", "62bc0d3d53132b3bec335f18"];
        user.password = await user.encryptPassword(password);

        await user.save();

        return res.status(201).json({
            message: "ลงทะเบียนเรียบร้อย",
        });
    } catch (error) {
        next(error);
    }
};

exports.login = async (req, res, next) => {
    try {
        const { email, password } = req.body;

        console.log("email " + email);
        console.log("password " + password);

        //check ว่ามีอีเมล์นี้ไม่ระบบหรือไม่
        const user = await User.findOne({ username: email });

        console.log('user : ', user.status)


        if (!user) {
            console.log("error 1");
            const error = new Error("ไม่พบผู้ใช้งานในระบบ");
            error.statusCode = 404;
            throw error;
        }

        if (user.status != 'use') {
            console.log("error 2");
            const error = new Error("User ของท่านอยู่ระหว่างรออนุมัติ");
            error.statusCode = 404;
            throw error;
        }

        //ตรวจสอบรหัสผ่านว่าตรงกันหรือไม่ ถ้าไม่ตรง (false) ให้โยน error ออกไป
        const isValid = await user.checkPassword(password);
        if (!isValid) {
            console.log("error 2");
            const error = new Error("รหัสผ่านไม่ถูกต้อง");
            error.statusCode = 401;
            throw error;
        }



        //สร้าง token
        const token = await jwt.sign(
            {
                id: user._id,
                role: user.role,
            },
            config.JWT_SECRET,
            { expiresIn: "5 days" }
        );

        //decode วันหมดอายุ
        const expires_in = jwt.decode(token);


        return res.status(200).json({
            result: true,
            access_token: token,
            expires_in: expires_in.exp,
            token_type: "Bearer",
            role:user.role,
            id:user._id,
            name:user.name,
            email:user.email,
            credit_limit:user.credit_limit,
            auction_board:user.auction_board,
            video: process.env.PATH_VIDEO,
            image: process.env.PATH_IMAGE
        });

    } catch (error) {
        next(error);
    }
}
