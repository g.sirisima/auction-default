const Product = require("../models/product");
const User = require('../models/user')
const Lot = require("../models/lot");
const Room = require("../models/room");
const { validationResult } = require("express-validator");

exports.index = async (req, res, next) => {
  const data = await Room.find({ active: { $ne: 'delete' } });
  res.render("admin/room/index", {
    data: data,
    title: "Room",
    nav: "Room List",
  });
}

exports.create = async (req, res, next) => {
  const item = {
    name:''
  }
  res.render("admin/room/create", {
    title: "Room",
    nav: "Room create",
    action:'create',
    id:'',
    item:item
  });
}

exports.create_db = async (req, res, next) => {
    console.log('req user',req.user);
    console.log('req body',req.body);
    var {name,user_id} = req.body

    let room = new Room()
    room.name = name
    room.user_created = user_id
    room.created_at = Date.now()

    await room.save();

    return res.status(201).json({result:true,message:'Create room success!',item:room});
}

exports.edit = async (req, res, next) => {
  const id = req.params.id
  const item = await Room.findOne({_id:id})
  console.log('item room ',item)
  res.render("admin/room/create", {
    title: "Room",
    nav: "Room edit",
    action:'edit',
    id:id,
    item:item,
  });
}

exports.edit_db = async (req, res, next) => {
  const {
    name,
    id,
    user_id
  } = req.body

  const action = await Room.findOneAndUpdate(
    {_id:id},
    {
      name:name,
      user_updated:user_id,
      updated_at:Date.now(),
    }
  )

  console.log('action update ',action)
  res.status(201).json({
    message: "Update data success!!",
    result: true,
  })
}

exports.delete = async (req, res, next) => {
  const {
    name,
    id,
    user_id
  } = req.body

  const action = await Room.findOneAndUpdate(
    {_id:id},
    {
      active:'delete',
      user_updated:user_id,
      updated_at:Date.now(),
    }
  )

  console.log('action update ',action)
  res.status(201).json({
    message: "Update data success!!",
    result: true,
  })
}

  
