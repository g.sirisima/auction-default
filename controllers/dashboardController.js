const User = require('../models/user')
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
const config = require('../config/index');
const Product = require('../models/product')
const Lot = require("../models/lot")

exports.index = (req, res, next) => {
    res.render('dashboard',
        {
            title: "Dashboard",
            nav: "Dashboard",
        }
    )
}

exports.load_lot = async (req, res, next) => {

    const lots = await Lot.find();

    res.status(200).json(
        {
            result: true,
            lots: lots
        }
    )
}