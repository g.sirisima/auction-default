const Product = require("../models/product");
const BidConfig = require('../models/config_bid')
const Lot = require("../models/lot");
const Log = require("../models/log");
const {
  validationResult
} = require("express-validator");
const moment = require('moment')

exports.index = async (req, res, next) => {
  let findObject = {};
  const product = await Product.find({
    status: true
  });
  res.render("admin/auctioneer/index", {
    product_id: req.params.room,
    data: product,
    findObject: findObject,
    title: "Auctioneer",
    nav: "Auctioneer Lot",
  });
};

exports.board = async (req, res, next) => {
  console.log('req.params.id : ', req.params.id)

  const lot = await Lot.findOne({
    _id: req.params.id
  });

  let product = {}
  
  if (lot.product_id) {
    console.log(1)
    product = await Product.findOne({
      _id: lot.product_id
    })

    if(!product){
      console.log(3)
      product = await Product.findOne({
        lot: req.params.id
      }).sort({
        orderNo: 1
      })
    }
  } else {
    console.log(2)
    product = await Product.findOne({
      lot: req.params.id
    }).sort({
      orderNo: 1
    })
  }

  res.render("admin/auctioneer/board", {
    lotId: req.params.id,
    lot: lot,
    product: product,
    title: "Auctioneer",
    nav: "Auctioneer List",
  });
};

exports.board2 = async (req, res, next) => {
  console.log('req.params.id : ', req.params.id)

  const lot = await Lot.findOne({
    _id: req.params.id
  });

  let product = {}
  if (lot.product_id) {
    console.log(1)
    
    product = await Product.findOne({
      _id: lot.product_id
    })

    if(!product){
      console.log(3)
      product = await Product.findOne({
        lot: req.params.id
      }).sort({
        orderNo: 1
      })
    }

  } else {

    console.log(2)
    product = await Product.findOne({
      lot: req.params.id
    }).sort({
      orderNo: 1
    })

  }

  res.render("admin/auctioneer/board2", {
    lotId: req.params.id,
    lot: lot,
    product: product,
    title: "Auctioneer",
    nav: "Auctioneer List",
  });
};

exports.auction_lot = async (req, res, next) => {

  const lot = await Lot.findOne({
    _id: req.params.id
  });

  const products = await Product.find({
    lot: req.params.id,
    active: {
      $ne: 'delete'
    }
  }).sort({orderNo: 1})

  let product = {}
  
  if (lot.product_id) {

    console.log(1)
    product = await Product.findOne({
      _id: lot.product_id
    })

    if(!product){
      console.log(3)
      product = await Product.findOne({
        lot: req.params.id
      }).sort({
        orderNo: 1
      })
    }

  } else {
    console.log(2)
    product = await Product.findOne({
      lot: req.params.id
    }).sort({
      orderNo: 1
    })
  }

  const send = {
    lot: lot,
    products: products,
    product: product,
    title: "Auctioneer",
    nav: "Auctioneer List",
  }
  //res.json(send)

  res.render("admin/auctioneer/room_auction", send);
};


exports.load_product = async (req, res, next) => {

  const product = await Product.findOne({
    _id: req.body.product_id
  }).sort({orderNo: 1})



  if (req.body.type == 'set') {


    await Lot.updateOne({
      _id: product.lot
    }, {
      product_id: req.body.product_id,
      updated_at: moment().format('YYYY/MM/DD HH:mm:ss'),
      user_updated: req.user._id

    });
  }

  res.status(200).json({
    result: true,
    product: product,
  });
};


exports.confirm_product = async (req, res, next) => {

  const update = await Product.updateOne({
    _id: req.body.product_id
  }, {
    finishPrice: req.body.product_price_finish,
    userWin: req.body.product_win,
    updated_at: moment().format('YYYY/MM/DD HH:mm:ss'),
    user_updated: req.user._id

  });

  const product = await Product.findOne({
    _id: req.body.product_id
  })

  //console.log("product : ", product)

  res.status(201).json({
    result: true,
    product: product,
    update: update
  });
};