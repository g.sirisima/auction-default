const Product = require("../models/product")
const User = require('../models/user')
const Lot = require("../models/lot")
const Room = require("../models/room")
const { validationResult } = require("express-validator")

exports.index = async (req, res, next) => {
  // const user = await User.find({ "role": { "$ne": 'admin' } })
  // .populate('room','name _id')
  // .populate('product','name _id')
  // .populate('lot','name _id')

  const user = []

  res.render("admin/member/index", {
    product_id: req.params.room,
    data: user,
    title: "User",
    nav: "User List",
  });
  //res.json(user);
}

exports.index_post = async (req, res, next) => {
  try {
    const user = await User.aggregate([
      {
          $match: {"role": { "$ne": 'admin'},"active": { "$ne": 'delete'}}
      },
      {
          $lookup: {
              from: "rooms", // collection name in db
              localField: "room",
              foreignField: "_id",
              as: "users_room"
          },
      },
      {
        $lookup: {
            from: "lots", // collection name in db
            localField: "lot",
            foreignField: "_id",
            as: "users_lot"
        },
      },
      {
        $lookup: {
            from: "products", // collection name in db
            localField: "product",
            foreignField: "_id",
            as: "users_product"
        },
      }
    ])
    return res.status(200).json({
      data: {
        user:user
      },
      result:true
    }); 
  } catch (error) {
    next(error); 
  }
}

exports.create = async (req, res, next) => {
  const room = await Room.find({"active": { "$ne": 'delete'}});
  const lot = await Lot.find({"active": { "$ne": 'delete'}});
  res.render("admin/member/create", {
    title: "User",
    nav: "User Create",
    data:{
      room:room,
      lot:lot,
      
    },
    action:'create'
  });
}

exports.create_post = async (req, res, next) => {
  console.log('asd',req.body)
  const { name, username, password,auction_board,user_id,active,credit_limit } = req.body;

  try {
    // const roomEx = await User.findOne({room: room});
    // if (roomEx) {
    //     const error = new Error('Room is duplicate');
    //     error.statusCode = 400;
    //     throw error;
    // }

    let user = new User()
    user.name = name
    user.auction_board = auction_board
    user.username = username
    user.password = await user.encryptPassword(password)
    user.user_created = user_id
    user.active = active
    user.credit_limit = parseInt(credit_limit)
    user.created_at = Date.now()
  
    await user.save();
  
    console.log(user)
  
    return res.status(201).json({
      message: "Save success!",
      result: true,
      item:user
    })
  } catch (error) {
       next(error)
  }
}

exports.product = async (req, res, next) => {
  console.log(req.body)

  var product = await Product.find({lot:req.body.lot,"active": { "$ne": 'delete'}})

  return res.status(201).json({
    data: {
      product:product
    },
    result: true,
  })
}

exports.edit_form = async (req, res, next) => {
  const room = await Room.find({});
  const lot = await Lot.find({});
  const id = req.params.id
  res.render("admin/member/create", {
    title: "User",
    nav: "User Create",
    data:{
      room:room,
      lot:lot,
    },
    action:'edit',
    id:id
  });
}

exports.edit_data = async (req, res, next) => {
  console.log(req.body)

  var item = await User.findOne({_id:req.body.id})

  return res.status(201).json({
    data: item,
    result: true,
  })
}

exports.edit_db = async (req, res, next) => {
  console.log(req.body)

  const {name,room,lot,product,user_id,id,new_password,active,auction_board,credit_limit} = req.body
  
  let user = await User.findById(id);

  user.name = name
  user.room = room
  user.lot = lot
  user.product = product
  user.auction_board= auction_board
  user.credit_limit= credit_limit
  user.active= active
  user.user_updated = user_id
  user.updated_at = Date.now()

  if(new_password!=''){
    user.password = await user.encryptPassword(new_password)
  }

  console.log(user)

  await user.save(function (err, result) {
    if (err) {
        return res.status(400).json({
          result: false,
        });
    } else {
        return res.status(201).json({
          result: true,
        });
    }
  });
}

exports.delete = async (req, res, next) => {
  // const action = await User.findOneAndRemove({_id:req.body.id})
  // console.log(action)

  const action = await User.findOdeneAndUpdate(
    {_id:req.body.id},
    {
      active:'delete'
    }
  )
  
  res.status(201).json({
    message: "Detele user success!!",
    result: true,
    action:action
  })
};





