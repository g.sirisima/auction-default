const User = require('../models/user')
const Product = require('../models/product')
const Lot = require("../models/lot")
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
const config = require('../config/index');
const mongoose = require('mongoose');

exports.index = async (req, res, next) => {
    const product = await Product.find({});
    res.render('client/home', { title: 'Home', nav: 'Home',data:product })
}

exports.load_product = async (req, res, next) => {
    try {
        const lot = await Lot.find({status:'open'});


    
        return res.status(200).json({
            lot:lot,
            result:true
        }); 
    } catch (error) {
       next(error); 
    }
}