const Cron = require('../models/cron')
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
const config = require('../config/index');
const request = require('request').defaults({rejectUnauthorized:false});
let jobs = [];
var cron = require('node-cron');
// const mysql      = require('mysql');

// const connection = mysql.createConnection({
// 	host     : 'localhost',
// 	port     : '8111',
// 	user     : 'root',
// 	password : '',
// 	database : 'dw'
// });


// exports.datasql = async (req, res, next) => {
// 	console.log(req.body);
// 	connection.connect();
// 	connection.query('select * from ar_ranking', function (error, results, fields) {
// 		console.log(results);
// 	});
// 	connection.end();
// 	return res.status(200).json({result:true});
// }

exports.save = async (req, res, next) => {
	console.log(req.body);
	let cron = new Cron();
	let time = req.body.minute+' '+req.body.hour+' '+req.body.dayofmonth+' '+req.body.month+' '+req.body.dayofweek;

	cron.time = time;
	cron.comment = req.body.comment;
	cron.url = req.body.url;

	await cron.save();
	
	return res.status(200).json({result:true});
}

exports.del = async (req, res, next) => {
	console.log(req.body);
	const action = await Cron.deleteOne({_id:req.body.id});
	return res.status(200).json({result:true,action:action});
}

exports.load = async (req,res,next)=>{

	const list = await Cron.find();
	return res.status(200).json({result:true,data:list});
}


exports.findone = async (req,res,next)=>{

	const list = await Cron.findOne({_id:req.body.id});
	return res.status(200).json({result:true,data:list});
}

exports.action = async (req,res,next)=>{

	const list = await Cron.findOne({_id:req.body.id});
	//console.log(list);
	let update_cron = false;
	
	if(list.action){
		console.log('cron stop');
		update_cron = false;

		if (jobs[req.body.id]) {
			console.log('have job');
			// console.log(jobs[0]);
			let ss = jobs[req.body.id];
			ss.stop();
			delete jobs[req.body.id];
		}
	}else{
		console.log('cron start');
		update_cron = true;
		let ss = cron.schedule(list.time, () =>  {
			console.log('jobs : '+req.body.id);
			active_url(list.url);
		}, {
			scheduled: false
		});
		ss.start();
		jobs[req.body.id] = ss;
	}

	console.log(jobs);
	console.log('inde '+jobs.indexOf(req.body.id));
	console.log('data '+jobs[0]);

	//console.log(UserAction());

	const update = await Cron.updateOne({_id:req.body.id},{action:update_cron});
	return res.status(200).json({result:true,data:list});
}

exports.ping = async (req, res, next) => {
	const action = true
	console.log('id : '+req.body.id);
	console.log('url : '+req.body.url);
	active_url(req.body.url+'/'+req.body.id);
	return res.status(200).json({result:true,action:action});
}

function active_url(url) {
	//https://auctlive.auct.co.th/index.php/rank/save
	//http://auct.co.th/php5/ci/ci_service_php5/index.php/sms/insert_log_nodejs
	console.log('url '+url);
	request(url, (err, res, body) => {
		if (err) { return console.log(err); }

		console.log(body);
	});
}

