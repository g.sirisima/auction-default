const Product = require("../models/product");
const User = require('../models/user')
const Lot = require("../models/lot");
const Room = require("../models/room");
const { validationResult } = require("express-validator");

exports.index = async (req, res, next) => {
  const data = await Room.find({});
  res.render("client/roundResult", {
    data: data,
    title: "My Results",
    nav: "My Results",
  });
}
