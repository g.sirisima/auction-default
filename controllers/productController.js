const Product = require("../models/product")

const Lot = require("../models/lot")



const {
  validationResult
} = require("express-validator")

const moment = require('moment')

exports.index = async (req, res, next) => {
  let findObject = {};
  const product = await Product.find(findObject);
  res.render("admin/product/index", {
    product_id: req.params.room,
    data: product,
    findObject: findObject,
    title: "Product",
    nav: "Product List",
  });
};

exports.product_list_db = async (req, res, next) => {
  try {

    let find = {
      active: {
        $ne: 'delete'
      }
    }

    if (req.body.lotId) {
      find.lot = req.body.lotId
    }

    console.log('find : ', find)
    const data = await Product.find(find).sort({
      orderNo: 1
    })

    const lot = await Lot.findOne({
      _id: req.body.lotId
    })

    return res.status(200).json({
      message: "success",
      data: data,
      lot: lot,
      result: true,
    });

  } catch (error) {
    next(error);
  }
};

exports.data_db = async (req, res, next) => {
  try {
    const id = req.body.id
    const data = await Product.findOne({
      _id: id
    });

    console.log('data db', data)
    return res.status(201).json({
      message: "success",
      data: data,
      result: true,
    });
  } catch (error) {
    next(error);
  }
};

exports.edit_db = async (req, res, next) => {
  const {
    name,
    detail,
    date_start,
    date_end,
    status,
    type,
    method_start,
    startPrice,
    step,
    lot,
    round_countdown,
    round_max,
    province,
    user_id,
    id,
  } = req.body

  let updatedb = {
    name: name,
    startPrice: startPrice,
    date_start: date_start,
    date_end: date_end,
    status: status,
    province: province,
    type: type,
    method_start: method_start,
    step: step,
    lot: lot,
    round_countdown: round_countdown,
    round_max: round_max,
    detail: detail,
    updated_at: Date.now(),
    user_updated: user_id
  }

  let pictures = getpictures(req.files)
  if (pictures.length > 0) {
    updatedb.pictures = pictures
  }


  const action = await Product.findOneAndUpdate({
    _id: id
  }, updatedb)

  console.log('action update ', action)
  res.status(201).json({
    message: "Update Success!!",
    result: true,
  })
};

exports.create_db = async (req, res, next) => {
  try {
    const lot = await Lot.findOne({
      _id: req.body.lot
    });


    let product = new Product();
    product.name = req.body.name
    product.status = req.body.status
    product.orderNo = req.body.orderNo
    product.detail = req.body.detail
    product.lot = req.body.lot
    product.startPrice = req.body.startPrice
    product.cur_Price = req.body.startPrice
    product.type = req.body.type
    product.step = req.body.step
    product.pictures = getpictures(req.files)
    product.user_created = req.user._id
    product.created_at = Date.now()
    product.date_end = lot.date_end
    product.date_start = lot.date_start

    console.log('product : ', product)

    await product.save();

    return res.status(201).json({
      message: "Create success!!",
      result: true,
      item: product
    });

  } catch (error) {
    next(error);
  }
};


exports.delete = async (req, res, next) => {
  const {
    name,
    id,
    user_id
  } = req.body

  const action = await Product.findOneAndUpdate({
    _id: id
  }, {
    active: 'delete',
    user_updated: user_id,
    updated_at: Date.now(),
  })

  console.log('action update ', action)
  res.status(201).json({
    message: "Update data success!!",
    result: true,
  })
};

function getpictures(files) {
  let result = []

  for (const i in files) {

    result.push(files[i].filename)


  }

  return result
}

exports.create = async (req, res, next) => {
  const lot = await Lot.find({
    "active": {
      "$ne": 'delete'
    }
  });
  res.render("admin/product/create", {
    title: "Product",
    nav: "Product Create",
    lot: lot,
    action: 'create'
  });
};


exports.edit = async (req, res, next) => {
  const lot = await Lot.find({})
  const id = req.params.id
  console.log('product id ' + id)
  console.log('moment : ', moment(Date.now()).format('YYYY/MM/DD HH:mm'))

  res.render("admin/product/create", {
    title: "Product",
    nav: "Product Edit",
    lot: lot,
    action: 'edit',
    id: id
  });
};

//lot

exports.products = async (req, res, next) => {

  console.log('lot id ', req.params.id)

  res.render("admin/lot/products", {
    title: "Lot",
    nav: "Lot Products",
    lotId: req.params.id
  });

};

exports.lot_list = async (req, res, next) => {
  res.render("admin/lot/list", {
    title: "Lot",
    nav: "Lot List"
  });
};

exports.lot_list_db = async (req, res, next) => {
  try {
    const data = await Lot.find({});
    return res.status(200).json({
      message: "success",
      data: data,
      result: true,
    });
  } catch (error) {
    next(error);
  }
};

exports.lot_create = async (req, res, next) => {
  res.render("admin/lot/create", {
    title: "Lot Create",
    nav: "Lot Create",
    action: 'create'
  });
};

exports.lot_create_db = async (req, res, next) => {
  try {
    let lot = new Lot();
    lot.name = req.body.name;
    lot.detail = req.body.detail;
    lot.status = req.body.status;
    lot.date_start = req.body.date_start;
    lot.date_end = req.body.date_end;
    lot.user_created = req.body.user_created
    lot.created_at = Date.now()
    console.log(lot);

    await lot.save();

    return res.status(201).json({
      message: "Create data success!",
      result: true,
      item: lot
    });
  } catch (error) {
    next(error);
  }
};

exports.edit_lot = async (req, res, next) => {
  const id = req.params.id

  res.render("admin/lot/create", {
    title: "Lot",
    nav: "Lot Edit",
    action: 'edit',
    id: id
  });
};

exports.lot_db = async (req, res, next) => {
  try {
    const id = req.body.id
    const data = await Lot.findOne({
      _id: id
    });
    console.log('data db', data)
    return res.status(201).json({
      message: "success",
      data: data,
      result: true,
    });
  } catch (error) {
    next(error);
  }
};

exports.lot_edit_db = async (req, res, next) => {
  const {
    name,
    detail,
    date_start,
    date_end,
    user_updated,
    status
  } = req.body

  const action = await Lot.findOneAndUpdate({
    _id: req.body.id
  }, {
    name: name,
    detail: detail,
    date_start: date_start,
    date_end: date_end,
    status: status,
    updated_at: Date.now(),
    user_updated: user_updated
  })

  console.log('action update ', action)
  res.status(201).json({
    message: "Update lot success!!",
    result: true,
  })
};

exports.lot_delete_db = async (req, res, next) => {
  const {
    name,
    id,
    user_id
  } = req.body

  const action = await Lot.findOneAndUpdate({
    _id: id
  }, {
    active: 'delete',
    user_updated: user_id,
    updated_at: Date.now(),
  })

  console.log('action update ', action)
  res.status(201).json({
    message: "Update data success!!",
    result: true,
  })
};