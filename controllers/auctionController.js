const User = require('../models/user')
const Product = require('../models/product')
const Lot = require('../models/lot')
const {
    validationResult
} = require('express-validator');
const jwt = require('jsonwebtoken');
const config = require('../config/index');
const mongoose = require('mongoose');

exports.room = async (req, res, next) => {
    console.log('req.params.id : ', req.params.id)

    const lot = await Lot.findOne({
        _id: req.params.id
    });

    let product = {}
    if (lot.product_id) {
        console.log(1)

        product = await Product.findOne({
            _id: lot.product_id
        })

        if (!product) {
            console.log(3)
            product = await Product.findOne({
                lot: req.params.id
            }).sort({
                orderNo: 1
            })
        }

    } else {

        console.log(2)
        product = await Product.findOne({
            lot: req.params.id
        }).sort({
            orderNo: 1
        })

    }

    res.render('client/room', {
        title: 'Home',
        nav: 'Home',
        lotId: req.params.id,
        lot: lot,
        product: product,
    })
}

exports.Slug2 = async (req, res, next) => {
    let findObject = {}
    const product = await Product.findOne({
        lot: mongoose.Types.ObjectId(req.params.id)
    });

    res.render('client/AuctionSlug2', {
        title: 'Home',
        nav: 'Home',
        data: product,
    })
}

exports.product = async (req, res, next) => {

    try {
        var result = false
        const product = await Product.findOne({
            _id: req.body.id
        });
        if (product) {
            result = true
        }
        return res.status(200).json({
            data: product,
            result: result
        });
    } catch (error) {
        next(error);
    }
}

exports.getProduct = async (req, res, next) => {
    try {
        const user = await User.findOne({
            _id: req.body.user_id
        })
        console.log(user)
        const product = await Product.find({
            _id: user.product
        });

        return res.status(200).json({
            data: product,
            result: true
        });
    } catch (error) {
        next(error);
    }
}