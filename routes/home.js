var express = require("express");
var router = express.Router();
const passportJWT = require("../middleware/passportJWT");
const homeController = require('../controllers/homeController');
// /* GET home page. */
// router.get("/",function (req, res, next) {

//   res.render("client/home", { title: "Hey", message: "Hello there!" });
// });

router.get("/", homeController.index);

// router.get("/AuctionSlug/:room", function (req, res, next) {
//   res.render('bid', { product_id: req.params.room,title: 'Hey', message: 'Hello there!' })
// });

router.post("/load_product", homeController.load_product);


router.get("/list", [passportJWT.isLogin], function (req, res, next) {
  res.json({ title: "Hey", message: "Hello there!" });
});

module.exports = router;
