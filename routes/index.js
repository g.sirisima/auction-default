var express = require("express");
var router = express.Router();
const passportJWT = require("../middleware/passportJWT");
const indexController = require("../controllers/indexController");

/* GET home page. */
router.get("/", indexController.index)

router.get("/check_login", indexController.check_login);


router.get("/authen", indexController.authen);
// router.get("/chat", function (req, res, next) {
//   res.render('chat', { title: 'Hey', message: 'Hello there!' })
// });

// router.get("/bid/:room", function (req, res, next) {
//   res.render('bid', { product_id: req.params.room,title: 'Hey', message: 'Hello there!' })
// });

// // router.get("/admin/product", function (req, res, next) {
// //   res.render('admin/product/index', { product_id: req.params.room,title: 'Hey', message: 'Hello there!' })
// // });

// router.get("/admin/product/create", function (req, res, next) {
//   res.render('admin/product/create', { product_id: req.params.room,title: 'Hey', message: 'Hello there!' })
// });

// router.get("/logout", function (req, res, next) {
//   res.render('index', { title: 'Hey', message: 'Hello there!' })
// });

module.exports = router;
