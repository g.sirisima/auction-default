var express = require("express");
const router = express.Router();
const {
    body
} = require("express-validator");
const productController = require("../controllers/productController");
const passportJWT = require("../middleware/passportJWT");
const checkAdmin = require("../middleware/checkAdmin");

const multer = require('multer')
const storage = multer.diskStorage({
    destination: function (req, file, callback) {

        callback(null, 'public/uploads/')
    },
    filename: function (req, file, callback) {

        const type = file.mimetype.split("/");
        callback(null, file.fieldname +'-'+ (Math.random() + 1).toString(36).substring(7) + '-' + Date.now() + '.' + type[1])
    }
})

const upload = multer({
    storage
})
// [passportJWT.isLogin]
router.get("/", productController.index);

router.post("/", [passportJWT.isLogin, checkAdmin.isAdmin], productController.product_list_db);

router.get("/create", productController.create);

router.post("/create", [passportJWT.isLogin, checkAdmin.isAdmin, upload.any('pictures')], productController.create_db);

router.post("/data", [passportJWT.isLogin, checkAdmin.isAdmin], productController.data_db);

router.post("/delete", [passportJWT.isLogin, checkAdmin.isAdmin], productController.delete);

router.get("/edit/:id", productController.edit);

router.post("/edit", [passportJWT.isLogin, checkAdmin.isAdmin, upload.any('pictures')], productController.edit_db);

//lot

router.get("/lot/list", productController.lot_list);

router.post("/lot/list", [passportJWT.isLogin, checkAdmin.isAdmin], productController.lot_list_db);

router.get("/lot/create", productController.lot_create);

router.post("/lot/create", [passportJWT.isLogin, checkAdmin.isAdmin], productController.lot_create_db);

router.get("/lot/edit/:id", productController.edit_lot);

router.get("/lot/products/:id", productController.products);

router.post("/lot/data", [passportJWT.isLogin, checkAdmin.isAdmin], productController.lot_db);

router.post("/lot/edit", [passportJWT.isLogin, checkAdmin.isAdmin], productController.lot_edit_db);

router.post("/lot/delete", [passportJWT.isLogin, checkAdmin.isAdmin], productController.lot_delete_db);

module.exports = router;