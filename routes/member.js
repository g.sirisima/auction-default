var express = require("express");
const router = express.Router();
const { body } = require("express-validator");
const memberController = require("../controllers/memberController");
const passportJWT = require("../middleware/passportJWT");
const checkAdmin = require("../middleware/checkAdmin");
// [passportJWT.isLogin]
router.get("/", memberController.index)

router.post("/", memberController.index_post)

router.get("/create", memberController.create)

router.post("/create", memberController.create_post)

router.post("/product", memberController.product)

router.get("/edit/:id", memberController.edit_form)

router.post("/edit_data", memberController.edit_data)

router.post("/edit_db", memberController.edit_db)

router.post("/delete", memberController.delete)

module.exports = router;
