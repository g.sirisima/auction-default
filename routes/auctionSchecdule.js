var express = require("express");
const router = express.Router();
const { body } = require("express-validator");
const auctionSchecduleController = require("../controllers/auctionSchecduleController");
const passportJWT = require("../middleware/passportJWT");
const checkAdmin = require("../middleware/checkAdmin");
// [passportJWT.isLogin]
router.get("/", auctionSchecduleController.index);

module.exports = router;
