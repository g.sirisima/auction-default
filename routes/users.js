const express = require("express");
const router = express.Router();
const {
  body
} = require("express-validator");
const userController = require("../controllers/userController");
const passportJWT = require("../middleware/passportJWT");
const checkAdmin = require("../middleware/checkAdmin");

router.get("/", userController.index);

router.get("/register", userController.form_register);

router.get("/profile", userController.profile);

router.get(
  "/admin",
  [passportJWT.isLogin, checkAdmin.isAdmin],
  userController.admin
);

router.post('/register', [
  body('name').not().isEmpty().withMessage('กรุณาป้อนข้อมูลชื่อสกุลด้วย'),
  body('username').not().isEmpty().withMessage('กรุณากรอก username ด้วย').withMessage('รูปแบบอีเมล์ไม่ถูกต้อง'),
  body('password').not().isEmpty().withMessage('กรุณากรอกรหัสผ่านด้วย').isLength({ min: 3 }).withMessage('รหัสผ่านต้อง 3 ตัวอักษรขึ้นไป')
], userController.register);

router.post("/login", userController.login);

router.all("/check_login", [passportJWT.isLogin], userController.authen);

module.exports = router;