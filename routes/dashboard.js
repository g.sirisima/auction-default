const express = require('express');
const router = express.Router();
const { body } = require('express-validator');
const dashboardController = require('../controllers/dashboardController');
const passportJWT = require('../middleware/passportJWT');
const checkAdmin = require('../middleware/checkAdmin');

router.get('/', dashboardController.index);

router.post('/load_lot', dashboardController.load_lot);

module.exports = router;
