var express = require("express");
const router = express.Router();
const { body } = require("express-validator");
const roundResultController = require("../controllers/roundResultController");
const passportJWT = require("../middleware/passportJWT");
const checkAdmin = require("../middleware/checkAdmin");
// [passportJWT.isLogin]
router.get("/", roundResultController.index);

module.exports = router;
