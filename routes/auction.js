var express = require("express");
var router = express.Router();
const passportJWT = require("../middleware/passportJWT");
const auctionController = require('../controllers/auctionController');

// /* GET home page. */
// router.get("/",function (req, res, next) {

//   res.render("client/home", { title: "Hey", message: "Hello there!" });
// });

router.get("/:id", auctionController.room);


router.get("/room/:id", auctionController.Slug2);

// router.get("/AuctionSlug/:room", function (req, res, next) {
//   res.render('bid', { product_id: req.params.room,title: 'Hey', message: 'Hello there!' })
// });

router.post(
    "/product",
    auctionController.product
);

router.post(
    "/getProduct",
    auctionController.getProduct
);


module.exports = router;
