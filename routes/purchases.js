var express = require("express");
const router = express.Router();
const { body } = require("express-validator");
const purchasesController = require("../controllers/purchasesController");
const passportJWT = require("../middleware/passportJWT");
const checkAdmin = require("../middleware/checkAdmin");
// [passportJWT.isLogin]
router.get("/", purchasesController.index);

module.exports = router;
