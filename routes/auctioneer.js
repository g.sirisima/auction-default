var express = require("express");
const router = express.Router();
const {
    body
} = require("express-validator");
const auctioneerController = require("../controllers/auctioneerController");
const passportJWT = require("../middleware/passportJWT");
const checkAdmin = require("../middleware/checkAdmin");

router.get("/", auctioneerController.index)

router.get("/:id", auctioneerController.auction_lot)

router.get("/board/:id", auctioneerController.board)

router.get("/board2/:id", auctioneerController.board2)

router.post("/load_product", [passportJWT.isLogin], auctioneerController.load_product)

router.post("/confirm_product", [passportJWT.isLogin], auctioneerController.confirm_product)

module.exports = router;