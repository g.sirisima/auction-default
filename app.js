const express = require("express");
const bodyParser = require("body-parser");
const app = express();

var io = require("socket.io")();
app.io = io;

const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require("cors");
const passport = require("passport");
var moment = require('moment');


require("dotenv").config();

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const aboutRouter = require("./routes/about");
const dashboardRouter = require("./routes/dashboard");
const homeRouter = require("./routes/home");
const auctionRouter = require("./routes/auction");
const purchasesRouter = require("./routes/purchases");
const roundResultRouter = require("./routes/roundResult");
const auctionSchecduleRouter = require("./routes/auctionSchecdule");


const product = require('./models/product')

const roomRouter = require("./routes/room");
const productdRouter = require("./routes/product");
const auctioneernRouter = require("./routes/auctioneer");
const memberRouter = require("./routes/member");
//import middleware
const errorHandler = require("./middleware/errorHandler");

const mongoose = require("mongoose");

const monguri =
  "mongodb://" +
  process.env.MONGODB_USER +
  ":" +
  process.env.MONGODB_PASS +
  "@localhost:" +
  process.env.MONGODB_PORT +
  "/" +
  process.env.MONGODB_URI +
  "?authSource=admin";

console.log(monguri)

mongoose.connect(monguri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
});

app.set("view engine", "pug");

app.locals.env = process.env;

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
// app.set('view engine', 'html');

const Product = require('./models/product')
const BidConfig = require('./models/config_bid')
const User = require('./models/user')

io.on("connection", function (socket) {
  socket.on("chat message", (msg) => {
    io.emit("chat message", msg);
  });

  socket.on("countdown", (msg) => {
    io.emit("countdown", msg);
  });

  socket.on("disconnect", () => {
    console.log("user disconnected");
  });

  socket.on("join", function (room) {
    console.log("join room ", room);
    socket.join(room);
  });

  socket.on("leave", function (room) {
    console.log("leave room ", room);
    socket.leave(room);
  });

  socket.on("setBid", async function (room, datas, callback) {
  
    try {

      if (datas.status == 'bid') {
        bid(room, datas)
      } else if (datas.product_id) {
        move(room, datas)
      }


    } catch (err) {
      callback(err);
    }
  });

  async function move(room, datas) {
    console.log('move : ', datas)

    io.sockets.in(room).emit('setBid', {
      room,
      datas
    });
  }


  async function bid(room, datas) {
    console.log('bid : ', datas)
    await product.updateOne({
      _id: datas.product_id
    }, {
      cur_Price: datas.product_price_finish
    });


    io.sockets.in(room).emit('setBid', {
      room,
      datas
    });
  }

});

app.use(
  cors({
    origin: '*',
    credentials: true,
  })
);

app.use(passport.initialize());

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/about", aboutRouter);
app.use("/purchases", purchasesRouter);
app.use("/home", homeRouter);
app.use("/auction", auctionRouter);
app.use("/roundResult", roundResultRouter);
app.use("/auctionSchecdule", auctionSchecduleRouter);


app.use("/dashboard", dashboardRouter);
app.use("/admin/product", productdRouter);
app.use("/admin/member", memberRouter);
app.use("/admin/room", roomRouter);
app.use("/admin/auction", auctioneernRouter);


app.use(errorHandler);

module.exports = app;