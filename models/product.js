const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const schema = new Schema({
    name: {
        type: String,
        required: true
    },
    startPrice: {
        type: Number,
        required: true
    },
    finishPrice: {
        type: Number,
        default: ''
    },
    userWin: {
        type: String,
        default: ''
    },
    step: {
        type: Number,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    date_start: {
        type: Date,
        require: true
    },
    date_end: {
        type: Date,
        require: true
    },
    detail: {
        type: String
    },
    type: {
        type: String,
        require: true
    },
    lot: {
        type: String,
        require: true
    },
    cur_Price: {
        type: Number
    },
    active: {
        type: String,
        default: 'approve'
    },
    orderNo: {
        type: Number,
        require: true
    },
    pictures: {
        type: Array,
        require: true
    },
    created_at: Date,
    updated_at: Date,
    user_created: String,
    user_updated: String
}, {
    collection: 'products',
});

schema.virtual('products', {
    ref: 'Product', //ลิงก์ไปที่โมเดล Room
    localField: '_id', //_id ฟิลด์ของโมเดล Room (ไฟล์นี้)
    foreignField: 'product' //user ฟิลด์ของโมเดล room (fk)
});


module.exports = mongoose.model('Product', schema);