const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const schema = new Schema({
  name: {
    type: String,
    required: true
  },
  detail: {
    type: String
  },
  status: {
    type: String,
    required: true
  },
  date_start: {
    type: Date,
    require: true
  },
  date_end: {
    type: Date,
    require: true
  },
  product_id: {
    type: String,
    default: ''
  },
  created_at: {
    type: Date,
  },
  updated_at: {
    type: Date,
  },
  user_created: {
    type: String,
  },
  user_updated: {
    type: String,
  },
  active: {
    type: String,
    default: 'approve'
  },
}, {
  collection: "lots",
});

schema.virtual('lots', {
  ref: 'Lot', //ลิงก์ไปที่โมเดล Room
  localField: '_id', //_id ฟิลด์ของโมเดล Room (ไฟล์นี้)
  foreignField: 'lot' //user ฟิลด์ของโมเดล room (fk)
});


module.exports = mongoose.model("Lot", schema);