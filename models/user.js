const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const schema = new mongoose.Schema({
  name: { type: String, required: true, trim: true },
  username: {
    type: String,
    required: true,
    trim: true,
    unique: true,
    index: true,
  },
  password: { type: String, required: true, trim: true, minlength: 3 },
  status: { type: String, default: "wait" },
  role: { type: String, default: "member" },
  auction_board: { type: String, default: "" },
  credit_limit: {
    type: Number,
    default: 0
  },
  create_by: { type: String },
  create_date: { type: Date },
  created_at: {
    type: Date,
  },
  updated_at: {
      type: Date,
  },
  user_created: {
      type: String,
  },
  user_updated: {
      type: String,
  },
},{
  collection: 'users'
});



schema.methods.encryptPassword = async function(password) {
   const salt = await bcrypt.genSalt(5);
   const hashPassword = await bcrypt.hash(password, salt);
   return hashPassword;
}

schema.methods.checkPassword = async function(password) {
   const isValid = await bcrypt.compare(password, this.password);
   return isValid;
}

const user = mongoose.model('User', schema);

module.exports = user;