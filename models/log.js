const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const log = new Schema({
    detail: { type: String,required: true },
    create_by: { type: String, required: true },
    create_date: { type: Date , require: true },
    obj: Schema.Types.Mixed,
    product_id:String
},{
    collection: 'logs',
});


module.exports = mongoose.model('Log', log);