const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
 
const BidConfig = new Schema({
    StartPrice: { type: Number },
    EndPrice: { type: Number },
    StepBid: { type: Number },
},{
    collection: 'config_bid',
});



module.exports = mongoose.model('BidConfig', BidConfig);