const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const room = new Schema({
    name: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true,
        default: 'open'
    },
    active: {
        type: String,
        default: 'approve'
    },
    user_created: {
        type: String,
    },
    created_at: {
        type: Date,
    },
    user_updated: {
        type: String,
    },
    updated_at: {
        type: Date,
    },
}, {
    collection: 'rooms',
});

room.virtual('rooms', {
    ref: 'Room', //ลิงก์ไปที่โมเดล Room
    localField: '_id', //_id ฟิลด์ของโมเดล Room (ไฟล์นี้)
    foreignField: 'room' //user ฟิลด์ของโมเดล room (fk)
});


module.exports = mongoose.model('Room', room);