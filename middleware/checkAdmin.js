module.exports.isAdmin = (req, res, next) => {
    const { role } = req.user;
    console.log(req.user);
    console.log('is admin')
    if ( role === 'admin') {
        next();
    } else {
        return res.status(403).json({
            error: {
                message: 'Access denied'
            }
        });
    }
}